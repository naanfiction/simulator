To Run:

`nodemon --exec babel-node ./newengine/server.js`
`npm start` or `yarn start` does the same thing
`npm start-log` or `yarn start-log` will log output to a file

Not ideal, but sporadically we get these errors:

#1
➜ simulator git:(consolidate) ✗ yarn start-log
yarn run v1.15.2
warning package.json: No license field
$ nodemon --exec babel-node ./newengine/server.js > logs/$(date +"%Y-%m-%d.%R%z").txt
Browserslist: caniuse-lite is outdated. Please run:
npx browserslist@latest --update-db

Why you should do it regularly:
https://github.com/browserslist/browserslist#browsers-data-updating
Entity 2 not found
/Users/vshah/proj/simulator/newengine/state/EnvironmentManager.js:96
return \_ref3 = {}, \_defineProperty(\_ref3, \_Symbols["default"].directions.N, env[row - 1][col]), \_defineProperty(\_ref3, \_Symbols["default"].directions.E, env[row][col + 1]), \_defineProperty(\_ref3, \_Symbols["default"].directions.S, env[row + 1][col]), \_defineProperty(\_ref3, \_Symbols["default"].directions.W, env[row][col - 1]), \_ref3;
^

TypeError: Cannot read property 'undefined' of undefined
at Object.seeWhatIsNearby (/Users/vshah/proj/simulator/newengine/state/EnvironmentManager.js:68:29)
at forEach (/Users/vshah/proj/simulator/newengine/state/Updater.js:77:29)
at Array.forEach (<anonymous>)
at Object.dayCycle (/Users/vshah/proj/simulator/newengine/state/Updater.js:73:14)
at tickerCallback (/Users/vshah/proj/simulator/newengine/Engine.js:37:13)
at Timeout.tick [as \_onTimeout](/Users/vshah/proj/simulator/newengine/Ticker.js:32:5)
at listOnTimeout (internal/timers.js:554:17)
at processTimers (internal/timers.js:497:7)
^C

#2
➜ simulator git:(consolidate) ✗ yarn start-log
yarn run v1.15.2
warning package.json: No license field
$ nodemon --exec babel-node ./newengine/server.js > logs/$(date +"%Y-%m-%d.%R%z").txt
Browserslist: caniuse-lite is outdated. Please run:
npx browserslist@latest --update-db

Why you should do it regularly:
https://github.com/browserslist/browserslist#browsers-data-updating
Count Exceed: 5
Cannot find location to place WANDERER # 1, exiting
^C

---

Note that #2 occurs for any random entity, at various times. The 5 count is constant.
