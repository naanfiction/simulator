const Symbols = {
	render: {
		WALL: "#",
		EMPTY_SPACE: "-",
		WANDERER: "o",
		PLANT: "*",
		ROCK: "0",
		BABY: "b",
		PROCREATING: "G",
	},
	directions: {
		N: "N",
		E: "E",
		S: "S",
		W: "W",
	},
};

export default Symbols;
