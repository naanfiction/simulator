import log4js from "log4js";
// NEW 12/22/21
const DEFAULT_TICK_INTERVAL = 1000;

const Ticker = ({ ticksPerTurn, turnsToRun, tickInterval }) => {
	let intervalID;
	let dayNumber = 1;
	let currentTickCount = 1;
	let tickerCallback = () => {};
	const logger = log4js.getLogger();

	const resetDay = () => {
		dayNumber = 1;
	};

	const resetTickCount = () => {
		currentTickCount = 1;
	};

	const handleTiming = () => {
		// TODO: consider making this math simpler by keeping a 'total ticks' count
		if (dayNumber >= turnsToRun && currentTickCount >= ticksPerTurn) {
			stopClock();
		}
		currentTickCount += 1;
		// last action of this turn: mark the turn complete
		if (currentTickCount > ticksPerTurn) {
			dayNumber += 1; // we are on a new day
			resetTickCount();
		}
	};

	const tick = () => {
		tickerCallback({ currentTickCount, dayNumber });
		// At the end of the day, decide if we should stop
		handleTiming();
	};

	const stopClock = () => {
		logger.info("-- CLOCK STOPPING --");
		clearInterval(intervalID);
		resetDay();
		resetTickCount();
	};

	const pauseClock = () => {
		logger.info("-- CLOCK PAUSED --");
		clearInterval(intervalID);
	};

	const startClock = () => {
		logger.info("-- CLOCK STARTING --");
		intervalID = setInterval(tick, tickInterval || DEFAULT_TICK_INTERVAL);
	};

	const init = newCallback => {
		resetDay();
		resetTickCount();
		tickerCallback = newCallback;
	};

	return {
		startClock,
		pauseClock,
		stopClock,
		init,
	};
};

export default Ticker;
