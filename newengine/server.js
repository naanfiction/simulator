import express from "express";
import log4js from "log4js";
import Engine from "./Engine";
import config from "./conf";

// NEW 12/22/21
const app = express();
let engine;
app.get("/", (req, res) => {
	res.send("Hello World");
});

app.listen(4000, () => {
	log4js.configure("config/log4js.json");
	const logger = log4js.getLogger();
	logger.info("=========== Starting Engine ============");

	engine = Engine(config);
	engine.run();
	engine.printBoard();
});
