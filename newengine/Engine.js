import Ticker from "./Ticker";
import Renderer from "./Renderer";
import Updater from "./state/Updater";
// NEW 12/22/21
// ===================
/**
 * Engine Loop - tick, update entities, render everything
 * - Runs the Game.  Speaks to other things to handle work
 * Timer
 * Updater
 *  - Entity Manager
 *  - Environment Manager
 *  - Math Engine
 * Renderer
 *  - Rendering Engine
 *
 *
 * Updater ties everything together, it is the 'brains' and uses the managers and math engine to
 *          handle all the things
 * Renderer displays them
 *
 */

const Engine = config => {
	let ticker;
	let updater;
	let renderer;

	const initEngine = () => {
		ticker = Ticker(config);
		// Updater gets full config since it will pass to subcomponents
		updater = Updater(config);
		renderer = Renderer();
	};

	const onTick = ({ currentTickCount, dayNumber }) => {
		updater.dayCycle({ currentTickCount, dayNumber });
		renderer.tick({ currentTickCount, dayNumber });
		updater.nightCycle({ currentTickCount, dayNumber });
	};

	const run = () => {
		initEngine();

		ticker.init(onTick);

		updater.run();
		// NOTE: this doesn't really do anything right now
		renderer.run();
		ticker.startClock();
	};

	const printBoard = () => renderer.prettyPrintBoard();

	return {
		run,
		printBoard,
	};
};

export default Engine;
