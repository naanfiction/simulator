// NEW 12/22/21
import Symbols from "../supp/Symbols";
import log4js from "log4js";
const logger = log4js.getLogger();

// File to hold misc functions
export const findIdInEnvironment = (id, environment, height, width) => {
	for (let row = 1; row <= height; row++) {
		for (let col = 1; col <= width; col++) {
			if (environment[row][col]?.id === id) {
				return {
					row,
					col,
				};
			}
		}
	}
	logger.error(`Entity ${id} not found`);
	return {};
};

/**
 * fn: runInEnvironment
 *   runs a function for every cell in the environment
 * @param {*} funcToRun
 */
export const runInEnvironment = (env, height, width, funcToRun) => {
	for (let row = 1; row <= height; row++) {
		for (let col = 1; col <= width; col++) {
			funcToRun(env[row][col]);
		}
	}
};

/**
 * fn: createAvatarGivenEntity
 *  creates a brand new Avatar object for an entity.  New avatars always 'need to act' since they haven't
 *  yet acted. Also, they know nothing of what is nearby them.
 */
export const createAvatarGivenEntity = entity => {
	const { id, symbol, type } = entity;
	return {
		id,
		symbol,
		type,
		nearby: {},
	};
};

export const addToString = (obj, toString) => {
	const clonedObj = JSON.parse(JSON.stringify(obj));
	clonedObj.toString = toString;
	return clonedObj;
};

// export const avatarToStringed = entity => {
// 	const newAvatar = createAvatarGivenEntity(entity);
// 	return addToString(
// 		newAvatar,
// 		() => `${newAvatar.symbol} (${newAvatar.id})`
// 	);
// };

export const getEntityDisplayString = entity => {
	if (!entity.symbol && !entity.id) {
		throw new Error(
			`Missing data to display this entity's string version.  Symbol: ${entity.symbol}.  ID: ${entity.id}`
		);
	}
	return `${entity.symbol} (${entity.id})`;
};

// Keep this function at the end until I can update all of the tooling.  Right now the Babel parse will recognize the null coalesence but linting won't.
export const getSymbol = type => Symbols.render[type] ?? "0";
