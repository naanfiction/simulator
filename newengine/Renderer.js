import log4js from "log4js";
import store from "./redux/store";
// NEW 12/22/21

const Renderer = () => {
	const logger = log4js.getLogger();

	const run = () => {
		logger.info("INITIAL RENDER");
		prettyPrintAll(0, 0);
	};

	const prettyPrintBoard = () => {
		const height = store.getState().env.height; // height is rows
		const currentEnvironment = store.getState().env.environment;
		logger.info("Board: ");
		for (var r = 0; r < height + 2; r++) {
			logger.info(
				currentEnvironment[r]
					.map(cell =>
						typeof cell === "string" ? cell : cell?.symbol
					)
					.join(" ")
			);
		}
	};

	const prettyPrintAll = (currentTickCount, dayNumber) => {
		logger.info("####################");
		logger.info("Name: ", store.getState().game.name);
		logger.info(`Day: ${dayNumber} ... Current Tick: ${currentTickCount}`);
		prettyPrintEntities();
		prettyPrintBoard();
		prettyPrintLocationIndex();
	};

	const prettyPrintEntities = () => {
		const entities = store.getState().entities.entities;
		logger.info("Entities: ");
		logger.info(entities);
	};

	const prettyPrintLocationIndex = () => {
		const locationIndex = store.getState().env.locationIndex;
		logger.debug("Location Index: ");
		logger.debug(locationIndex);
	};

	const tick = ({ currentTickCount, dayNumber }) => {
		prettyPrintAll(currentTickCount, dayNumber);
	};

	const init = () => {};

	init();

	return {
		run,
		tick,
		prettyPrintEntities,
		prettyPrintBoard,
	};
};

export default Renderer;
