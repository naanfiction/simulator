import log4js from "log4js";

import Symbols from "../../supp/Symbols";

const logger = log4js.getLogger();

/* Functions for Environment Calc */
const integerDivide = (numerator, denominator) =>
	Math.floor(numerator / denominator);

const integerModulus = (numerator, denominator) =>
	Math.floor(numerator) % Math.floor(denominator);

const getRandomIntToMax = max => Math.floor(Math.random() * Math.floor(max));

const dirToIndAdj = {
	[Symbols.directions.N]: (row, col) => ({ row: row - 1, col }),
	[Symbols.directions.E]: (row, col) => ({ row, col: col + 1 }),
	[Symbols.directions.S]: (row, col) => ({ row: row + 1, col }),
	[Symbols.directions.W]: (row, col) => ({ row, col: col - 1 }),
};

const isCellEmpty = ({ row, col }, env) => {
	return env[row][col] === Symbols.render.EMPTY_SPACE;
};

/**
 * Find a random location that has neither an entity, nor a static object such as a Wall (or a Rock, in future iterations).
 * This precludes using the locationIndex since that data structure only accounts for entity locations.
 * @param {*} envState
 * @returns {row, col} of empty location
 */
const findRandomEmptyLocation = envState => {
	let count = 0;
	const rows = envState.height;
	const cols = envState.width;

	let row = getRandomIntToMax(rows);
	let col = getRandomIntToMax(cols);
	while (
		envState.environment[row][col] !== Symbols.render.EMPTY_SPACE &&
		count < 5
	) {
		if (envState.environment[row][col] !== Symbols.render.EMPTY_SPACE) {
			logger.trace(
				`There was already something at ${row}, ${col}.... ${JSON.stringify(
					envState.environment[row][col]
				)}`
			);
		}
		row = getRandomIntToMax(rows);
		col = getRandomIntToMax(cols);
		count++;
	}
	if (count >= 5) {
		logger.error("Count Exceed: ", count);
		return null;
	}
	logger.trace(
		`Random empty location for entity found at: ${row}, ${col}.  It has: ${envState.environment[row][col]}`
	);
	return { row, col };
};

const findAdjustedIndexInDirection = ({ row, col }, dir) => {
	if (!dir) {
		logger.warn("Warning, direction not found: ", { row, col, dir });
	}
	const adjFunc = dirToIndAdj[dir];
	if (!adjFunc) {
		logger.debug(`Adj Func not found. Dir: ${dir}.. R: ${row}, C:${col}`);
	}
	return adjFunc(row, col);
};

const findIndicesGivenEntityId = ({ env, rows, cols }, id) => {
	// const envState = store.getState().env;
	// const rows = envState.height;
	// const cols = envState.width;
	for (let row = 1; row <= rows; row++) {
		for (let col = 1; col <= cols; col++) {
			if (env[row][col]?.id === id) {
				return {
					row,
					col,
				};
			}
		}
	}
	logger.error(`Entity ${id} not found`);
	return {};
};

const pickRandomDirection = () => {
	var allDirections = Object.keys(Symbols.directions);
	var randomIndex = getRandomIntToMax(allDirections.length);
	// logger.trace(
	// 	`Picking random direction: ${allDirections}.. ${randomIndex}`,
	// 	Symbols.directions[allDirections[randomIndex]]
	// );
	return Symbols.directions[allDirections[randomIndex]];
};

const isAllNearbyFull = nearby => {
	return nearby
		? !Object.values(nearby).includes(Symbols.render.EMPTY_SPACE)
		: true;
};

/**
 * This function will return undefined if there is no empty space nearby.  Any caller of this function must keep this in mind.
 * @param {} entity
 * @returns null or one of Symbols.directions
 */
const pickEmptySpaceNearby = entity => {
	if (isAllNearbyFull(entity.nearby)) {
		return;
	}
	let spaceNearby = pickRandomDirection();
	while (Symbols.render.EMPTY_SPACE != entity.nearby[spaceNearby]) {
		spaceNearby = pickRandomDirection();
	}
	return spaceNearby;
};

/* End Functions for Environment Calc */

/**
 * MathEngine ... does math.  It performs the computations needed by the entities or the environments.  E.g.
 * Calculating where the next move is
 */
const MathEngine = (() => {
	const init = () => {};

	init();

	return {
		findRandomEmptyLocation,
		pickRandomDirection,
		isAllNearbyFull,
		pickEmptySpaceNearby,
		findIndicesGivenEntityId,
		findAdjustedIndexInDirection,
		isCellEmpty,
		integerDivide: integerDivide,
		integerModulus: integerModulus,
		getRandomIntToMax: getRandomIntToMax,
	};
})();

export default MathEngine;
