import log4js from "log4js";

import store from "../redux/store";
import actions from "../redux/actions";

import Symbols from "../../supp/Symbols";
import MathEngine from "./MathEngine";
import { getEntityDisplayString } from "../miscFunctions";

const logger = log4js.getLogger();
// NEW 12/23/21

const getEnvironment = () => {
	return store.getState().env.environment;
};

const getLocationIndex = () => {
	return store.getState().env.locationIndex;
};

const refreshAvatars = entities =>
	store.dispatch(actions.refreshAvatars(entities));

const createEnvironment = ({ height, width }) => {
	// height is rows
	// width is columns

	const environment = [];
	for (var r = 0; r < height + 2; r++) {
		environment[r] = [];
		for (var c = 0; c < width + 2; c++) {
			if (r % (height + 1) === 0 || c % (width + 1) === 0) {
				environment[r][c] = Symbols.render.WALL;
			} else {
				environment[r][c] = Symbols.render.EMPTY_SPACE;
			}
		}
	}
	return environment;
};

const isEntityTile = tile =>
	tile !== Symbols.render.EMPTY_SPACE && tile !== Symbols.render.WALL;

const findRandomEmptyLocationInCurrentEnv = () => {
	const envState = store.getState().env;
	return MathEngine.findRandomEmptyLocation(envState);
};

const seeWhatIsNearby = ({ row, col }) => {
	const env = getEnvironment();
	// If we're looking at a wall, skip
	if (
		!env?.[row]?.[col]?.symbol ||
		env?.[row]?.[col]?.symbol === Symbols.render.WALL
	) {
		logger.debug(
			`Found a wall or something here.  No direction. Row: ${row}, Col: ${col}, Cell: ${env?.[row]?.[col]}`
		);
		return;
	}
	return {
		[Symbols.directions.N]: env[row - 1][col],
		[Symbols.directions.E]: env[row][col + 1],
		[Symbols.directions.S]: env[row + 1][col],
		[Symbols.directions.W]: env[row][col - 1],
	};
};

/**
 * EnvironmentManager manages the environment, the 'world' and all of the entities in it.
 * This includes entities moving around and interacting with it.
 */

const EnvironmentManager = ({ envHeight, envWidth }) => {
	/* Api */

	const placeInEnvironment = (row, column, avatar) => {
		logger.debug(
			`Placing avatar ${getEntityDisplayString(
				avatar
			)} at ${row}, ${column}`
		);
		store.dispatch(
			actions.placeAvatarInEnvironment({ avatar, row, column })
		);
	};

	const findEntity = id => {
		const { environment, height, width } = store.getState().env;
		for (let row = 1; row <= height; row++) {
			for (let col = 1; col <= width; col++) {
				if (environment[row][col]?.id === id) {
					return {
						row,
						col,
					};
				}
			}
		}
		logger.error(`Entity ${id} not found`);
		return {};
	};

	const moveEntityWithId = (id, { dir }) => {
		const { environment, height, width } = store.getState().env;
		const entityCurrentLoc = MathEngine.findIndicesGivenEntityId(
			{
				env: environment,
				rows: height,
				cols: width,
			},
			id
		);
		const newCoords = MathEngine.findAdjustedIndexInDirection(
			entityCurrentLoc,
			dir
		);
		logger.debug(
			`Entity ${id} at ${JSON.stringify(entityCurrentLoc)} moves ${dir}`
		);
		moveToLoc({ oldCoords: entityCurrentLoc, newCoords });
	};

	const pickEmptySpaceNearCoords = ({ row, col }) => {
		const env = getEnvironment();
		let count = 0;
		let randomDir = MathEngine.pickRandomDirection();
		let adjustedIndex = MathEngine.findAdjustedIndexInDirection(
			{ row, col },
			randomDir
		);
		while (
			!MathEngine.isCellEmpty({ ...adjustedIndex }, env) &&
			count <= 5
		) {
			randomDir = MathEngine.pickRandomDirection();
			adjustedIndex = MathEngine.findAdjustedIndexInDirection(
				{ row, col },
				randomDir
			);
			count += 1;
		}
		if (count >= 5) {
			logger.debug(
				`Could not find an empty place near ${row}, ${col}.  Aborting this process`
			);
			return;
		}
		logger.debug(
			`Found this empty location near ${row}, ${col}: ${randomDir}.. ${JSON.stringify(
				adjustedIndex
			)}`
		);
		return { ...adjustedIndex };
	};

	const removeEntity = entity => {
		if (!entity.id) {
			logger.error(
				"Cannot remove entity with id that does not exist: ",
				entity
			);
		}
		const { id } = entity;
		store.dispatch(actions.death({ id }));
	};

	/* End Api */

	/* Functions */
	const moveToLoc = ({ oldCoords, newCoords }) => {
		const env = getEnvironment();
		if (MathEngine.isCellEmpty({ ...newCoords }, env)) {
			store.dispatch(actions.move({ oldCoords, newCoords }));
		} else {
			logger.warn("Warning, move location was already occupied", {
				oldCoords,
				newCoords,
			});
		}
	};
	/* End Functions */

	const init = () => {
		logger.info("Init Environment Manager");
		store.dispatch(
			actions.setEnvironment(
				createEnvironment({
					height: envHeight,
					width: envWidth,
				})
			)
		);
	};

	init();

	return {
		placeInEnvironment,
		getEnvironment,
		seeWhatIsNearby,
		findEntity,
		moveEntityWithId,
		refreshAvatars,
		findRandomEmptyLocationInCurrentEnv,
		pickEmptySpaceNearCoords,
		getLocationIndex,
		removeEntity,
	};
};

export default EnvironmentManager;
