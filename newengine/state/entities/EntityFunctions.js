// Use this resource.  Basically, want to pass state in every function
// https://medium.com/code-monkey/object-composition-in-javascript-2f9b9077b5e6
import log4js from "log4js";

import MathEngine from "../MathEngine";
import Symbols from "../../../supp/Symbols";

const logger = log4js.getLogger();
// NEW 12/22/21

// Implemented functions ======================

export const all = {
	look: (self, nearby) => {
		if (!self) {
			logger.error("Looking.  Self is not what was expected: ", self);
		}
		self.nearby = nearby;
	},
	think: self => {
		if (!self) {
			logger.error("Thinking.  Self is not what was expected: ", self);
		}
		logger.debug("Thinking.  Nothing happening.");
	},
};

export const thinkers = {
	wanderer: self => {
		const dir = MathEngine.pickEmptySpaceNearby(self);
		if (!dir) {
			logger.debug(`No space nearby Wanderer ${self}. Skipping action`);
			self.action = {
				name: "skip",
			};
		} else {
			self.action = {
				name: "move",
				params: {
					dir,
				},
			};
		}
		return self;
	},
	plant: self => {
		if (MathEngine.isAllNearbyFull(self.nearby)) {
			logger.warn("Warning: everything nearby plant was full");
			self.action = { name: "skip" };
			return;
		}
		// If baby, then 80% chance it will try to "Grow"
		if (self.symbol === Symbols.render.BABY) {
			let willGrowThisRound = MathEngine.getRandomIntToMax(5) < 4;
			if (willGrowThisRound) {
				self.action = { name: "grow" };
				return;
			}
			self.action = { name: "skip" };
			return;
		}
		// 20% chance to spawn a new one.  Can't procreate if already doing so
		var mightProcreateThisRound = MathEngine.getRandomIntToMax(5) === 1;

		// Babies and already-pregnants can't procreate
		// TODO: This is too much logic here; I don't like the thinking needing to know the symbols
		//-- will have to come up with a better solution for determining pregnancy- and baby- states
		if (
			mightProcreateThisRound &&
			!["G", Symbols.render.BABY].includes(self.symbol)
		) {
			self.symbol = "G";
			self.action = { name: "procreate" };
		} else {
			self.action = { name: "skip" };
		}
	},
};

// Can Functions ======================
export const canLook = fn => ({
	look: fn ? fn : all.look,
});

export const canThink = fn => ({
	think: fn ? fn : all.think,
});

export const canMove = entity => {
	if (!entity.allowedActions) {
		entity.allowedActions = [];
	}
	entity.allowedActions.push("move");
	return entity;
};

export const canSkip = entity => {
	if (!entity.allowedActions) {
		entity.allowedActions = [];
	}
	entity.allowedActions.push("skip");
	return entity;
};

export const canBreed = entity => {
	if (!entity.allowedActions) {
		entity.allowedActions = [];
	}
	entity.allowedActions.push("procreate");
	entity.allowedActions.push("deliver");
	return entity;
};

export const cannotBreed = entity => {
	if (!entity.allowedActions) {
		// NOTE: This might be weird for the 'cannotX' cases; since if there are no allowed actions then
		// clearly we don't need to remove. However, code-defense wise it makes a bit of sense since we want to make sure
		// the allowedActions is always a list, and plus it maintains consistency with other behavior functions.
		entity.allowedActions = [];
	}

	let rmInd = entity.allowedActions.findIndex(a => a === "procreate");
	if (rmInd !== -1) {
		entity.allowedActions.splice(rmInd, 1);
	}
	rmInd = entity.allowedActions.findIndex(a => a === "deliver");
	if (rmInd !== -1) {
		entity.allowedActions.splice(rmInd, 1);
	}
	return entity;
};

export const canGrow = entity => {
	if (!entity.allowedActions) {
		entity.allowedActions = [];
	}
	entity.allowedActions.push("grow");
	return entity;
};

//   toString() {
//     return `${this.symbol} -- ${this._id}`;
//   }
