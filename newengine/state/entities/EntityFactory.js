import log4js from "log4js";

import {
	canMove,
	canBreed,
	canLook,
	canSkip,
	canGrow,
	canThink,
	thinkers,
	all,
	cannotBreed,
} from "./EntityFunctions";

import ENTITY_TYPES from "../../EntityTypes";
import Symbols from "../../../supp/Symbols";
import MathEngine from "../MathEngine";

const logger = log4js.getLogger();

// NEW 12/22/21

// // look, move, and breed actions should be the same across all types of entities

const Wanderer = (isBaby = false) => {
	const newWanderer = {};
	const howToLook = canLook(all.look);
	const howToThink = canThink(thinkers.wanderer);
	let behaviorFns = Object.assign(
		newWanderer,
		howToLook,
		howToThink,
		canMove(newWanderer),
		canSkip(newWanderer)
	);
	if (isBaby) {
		behaviorFns = Object.assign(
			behaviorFns,
			canGrow(newWanderer),
			cannotBreed(newWanderer)
		);
	}

	return behaviorFns;
};

const Plant = (isBaby = false) => {
	const newPlant = {};
	const howToLook = canLook(all.look);
	const howToThink = canThink(thinkers.plant);
	let behaviorFns = Object.assign(
		newPlant,
		howToLook,
		howToThink,
		canBreed(newPlant),
		canSkip(newPlant)
	);
	if (isBaby) {
		behaviorFns = Object.assign(
			behaviorFns,
			canGrow(newPlant),
			cannotBreed(newPlant)
		);
	}
	return behaviorFns;
};

// const Rock = () => {
//   return Object.assign({}, isImmovable);
// };

// const Baby = () => {
//   return Object.assign(
//     {},
//     canLook
//     // canAct
//   );
//   //     canMove(state), //eventually move (and follow), but for now baby can only grow
//   //     canGrow(state)(type)
// };

// ============= FUNCTIONS ================
const createEntityBehaviorTypes = () => {
	const typeToBehavior = {};
	typeToBehavior[ENTITY_TYPES.WANDERER] = Wanderer();
	typeToBehavior[`${ENTITY_TYPES.WANDERER}-b`] = Wanderer(true);
	typeToBehavior[ENTITY_TYPES.PLANT] = Plant();
	typeToBehavior[`${ENTITY_TYPES.PLANT}-b`] = Plant(true);
	// typeToBehavior[ENTITY_TYPES.ROCK] = Rock();
	// typeToBehavior[ENTITY_TYPES.BABY] = Baby();
	return typeToBehavior;
};

// const createBabyEntity = ({ parentId, parent, type }) => {
//   const entity = EntityFactory.createNewEntity(type);
//   return { parentId, parent, ...entity };
// };

const createInitialEntityState = (id, type) => ({
	id,
	symbol: Symbols.render[type] ?? "0",
	type,
	// needsToAct: false, // when first created, do nothing that turn
	// nearby: {}
	initiative: MathEngine.getRandomIntToMax(10),
});

const createNewEntity = id => type => createInitialEntityState(id++, type);

// const createNewEntityToStringed = id => type => {
// 	const entity = createNewEntity(id, type);
// 	entity.toString = () =>
// 		`${entity.id}: (${entity.type}, ${entity.symbol}, ${entity.initiative})`;
//     return entity;
// };

const EntityFactory = (() => {
	let id = 0;

	return {
		createNewEntity: createNewEntity(id),
		createEntityBehaviorTypes,
		// createNewEntityToStringed,
		// createBabyEntity
	};
})();

export default EntityFactory;
