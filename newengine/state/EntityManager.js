import log4js from "log4js";

import store from "../redux/store";
import actions from "../redux/actions";
import EntityFactory from "./entities/EntityFactory";
import { getSymbol } from "../miscFunctions";

const logger = log4js.getLogger();

// NEW 12/23/21
/**
 * Entity Manager manages/manipulates the entities
 *
 * contains :
 * - EntityFactory to generate new entities
 * - list of entities
 */

const behaviorTypes = EntityFactory.createEntityBehaviorTypes();

// @Deprecated  -- ? may not need this function
const addToString = entity => {
	const newEntity = JSON.parse(JSON.stringify(entity));
	newEntity.toString = () =>
		`${entity.id}: (${entity.type}, ${entity.symbol}, ${entity.initiative})`;
	return newEntity;
};

const createNewEntity = type => {
	const newEntity = EntityFactory.createNewEntity(type);
	store.dispatch(actions.spontaneousBirth(newEntity));
	return newEntity;
};

const showEntities = () => store.getState().entities.entities;

const getEntitiesCopy = () => {
	// return JSON.parse(JSON.stringify(store.getState().entities.entities));
	return JSON.parse(JSON.stringify(showEntities()));
};

/**
 * fn: getEntityGivenId
 * return: a cloned version of the entity found.  It does need to be a copy, since the value
 * from the store itself is read-only.
 */
const getEntityGivenId = id => ({
	...showEntities().find(entity => entity.id === id),
});

const getEntityBehaviorFunctions = (entityType, isBaby = false) => {
	let keyToSearch = entityType;
	if (isBaby) {
		keyToSearch = `${entityType}-b`;
	}
	return behaviorTypes[keyToSearch];
};

const EntityManager = () => {
	let breedingEntityIds = [];

	const grow = entity => {
		// NOTE: or whatever MAX AGE is, here
		const { id, type } = entity;
		store.dispatch(actions.grow({ id, type }));
	};

	const procreate = id => {
		if (breedingEntityIds.includes(id)) {
			logger.error(`Entity ${id} is already breeding`);
			return;
		}
		breedingEntityIds.push(id);
		store.dispatch(actions.procreate({ id }));
	};

	const incubate = () => {
		store.dispatch(actions.incubate({ breedingEntityIds }));
	};

	const deliver = () => {
		logger.debug("DELIVERING: ", breedingEntityIds);

		/**
		 * of the breeding entities, we have some that are done incubating, and some that need to keep incubating
		 * New algorithm:
		 *  loop through entity ids, keep track of ones with new babies (done incubating)
		 *  once loops finishes, will have a second list of those that have incubated; this list is what needs to be processed, and then the ids removed from the oroginal list
		 *      for incubated list, loop through new list and :
		 *          create new baby entity, given parent's type
		 *          remove parent's incubation timer; reset parent's type
		 * remove the 'processed' ones from the breeding list
		 */

		const entities = getEntitiesCopy();
		const babiesAndParents = [];
		const keepThese = breedingEntityIds
			.map(breederId => {
				const parentEntityIndex = entities.findIndex(
					e => e.id === breederId
				);
				const parentEntity = entities[parentEntityIndex];
				if (parentEntity.incubate < 1) {
					logger.debug(
						`Parent ${parentEntity.id} needs to incubate more: ${parentEntity.incubate}`
					);
					// Still gotta incubate; return this id to process again in future
					return parentEntity.id;
				}

				// Same type as parent
				const baby = EntityFactory.createNewEntity(parentEntity.type);
				// Babies are always of symbol "b".  They'll get their type symbol after first day
				baby.symbol = "b";
				baby.age = 0;
				entities.push(baby);
				// Change the parent symbol and its incubate field.  Do this directly via array index
				entities[parentEntityIndex].symbol = getSymbol(
					parentEntity.type
				);
				delete entities[parentEntityIndex].incubate;
				babiesAndParents.push({ parentId: breederId, child: baby });
				return;
			})
			.filter(val => val !== undefined);
		breedingEntityIds = keepThese;
		// Save back to store
		store.dispatch(actions.setEntities(entities));
		return babiesAndParents;
	};

	// TODO: consider, should also keep entities in a list by type? e.g. map
	const run = () => {};

	const init = () => {
		logger.info("Init EntityManager");
	};

	init();

	return {
		run,
		createNewEntity,
		showEntities,
		getEntityGivenId,
		getEntityBehaviorFunctions,
		procreate,
		grow,
		incubate,
		deliver,
		// addToString,
	};
};

export default EntityManager;
