import log4js from "log4js";

// import store from "../redux/store";

import EntityManager from "./EntityManager";
import EnvironmentManager from "./EnvironmentManager";
// import MathEngine from "./MathEngine";
import EntityTypes from "../EntityTypes";
import Symbols from "../../supp/Symbols";

const logger = log4js.getLogger();

// NEW 12/22/21
const Updater = ({ initialPopulation, envHeight, envWidth }) => {
	let entityMgr;
	let envMgr;
	// List of babies born overnight, to be placed on the board next to their parents in the morning
	let newBabies = [];

	const populateWorld = () => {
		randomlyPopulateSpecies(
			EntityTypes.PLANT,
			initialPopulation[EntityTypes.PLANT]
		);
		randomlyPopulateSpecies(
			EntityTypes.WANDERER,
			initialPopulation[EntityTypes.WANDERER]
		);
	};

	// TODO: Can optimize here by getting all of the random spots at once and filling in all entities at the same time
	const randomlyPopulateSpecies = (type, maxNum) => {
		logger.info(`Populating ${maxNum} species of type ${type}`);
		let entityNumToPlace = 1;
		while (entityNumToPlace <= maxNum) {
			// call math engine to get a random place to put new entity
			const location = envMgr.findRandomEmptyLocationInCurrentEnv();
			if (!location) {
				logger.error(
					`Cannot find location to place ${type} # ${entityNumToPlace}, exiting`
				);
				process.exit(-1);
			}
			const { row, col } = location;
			// Entity Manager creates an entity, returns here
			const newEntity = entityMgr.createNewEntity(type);
			// call env manager and update location with entity
			envMgr.placeInEnvironment(row, col, newEntity);
			entityNumToPlace += 1;
		}
	};

	const getSortedEntitiesList = () => {
		const entities = [...entityMgr.showEntities()].sort(
			(firstEl, secondEl) => firstEl.initiative - secondEl.initiative
		);
		return entities;
	};

	const init = () => {
		logger.info("Init Updater");
		entityMgr = EntityManager();
		envMgr = EnvironmentManager({ envHeight, envWidth });
	};

	const entityActions = e => {
		// for each entity in order, ask env mgr where that entity is, and what's around it
		const entity = { ...e };
		const loc = envMgr.findEntity(entity.id);
		const nearby = envMgr.seeWhatIsNearby(loc);
		const behaviorFunctions = entityMgr.getEntityBehaviorFunctions(
			entity.type,
			entity.symbol === Symbols.render.BABY
		);
		// logger.trace(`Entity ${entity.id} acting.  Actions available are: `, {
		// 	behaviorFunctions,
		// });
		// env mgr returns this info, now we call behavior.look with nearby info; look modifies entity with its nearby info
		behaviorFunctions.look?.(entity, nearby);
		// call think now, passing the entity itself;  think will call the different thinks based on who thought them.  think also modifies self - appending the action object
		behaviorFunctions.think?.(entity);
		const action = entity.action;
		// TODO: actually limit the acitons, per the Entity Type Constructors
		//  typical possible actions: move, breed, <eat>, <fight>, no-op
		//    move needs env mgr;  breed needs ent mgr
		//    side note: no-op becomes especially useful when energy comes into play
		// and if action.name is in the behavior list for this entity .. or the 'allowed actions' list for the entity
		if (
			action.name &&
			!behaviorFunctions?.allowedActions?.includes(action.name)
		) {
			const messageObjs = { action, entity, behaviorFunctions };
			logger.debug(
				"This entity behavior irregularly.  Skipping.",
				messageObjs
			);
			logger.error(
				"ERROR: Trying to call an Entity behavior action which is not allowed by Entity.  Skipping",
				messageObjs
			);
			return;
		}
		// TODO: logger.debug(`Entity ${entity.id} taking ${action.name} action`);
		if (action.name === "move") {
			// call env manager's move function; .. or entity manager's procreate function; here is where you pick
			envMgr.moveEntityWithId(entity.id, { ...action.params });
		} else if (action.name === "procreate") {
			entityMgr.procreate(entity.id);
		} else if (action.name === "grow") {
			entityMgr.grow(entity);
		} else if (action.name === "skip") {
			// No-ops are all good
		}
	};

	const run = () => {
		populateWorld();
	};

	const placeBabies = () => {
		logger.debug("PLACING BABIES: ", newBabies);
		// Get baby's parent id
		// Look up parent's location in location index
		newBabies.forEach(babiesAndParents => {
			const { parentId, child } = babiesAndParents;
			const index = envMgr.getLocationIndex();
			const parentLocation = index[parentId];
			const childLocation = envMgr.pickEmptySpaceNearCoords({
				...parentLocation,
			});
			if (childLocation) {
				envMgr.placeInEnvironment(
					childLocation.row,
					childLocation.col,
					child
				);
			} else {
				// There is no place for the child, so remove it :/
				envMgr.removeEntity(child);
			}
		});
		newBabies = [];
	};
	// during the day, things happen, the entities move and interact
	const dayCycle = ({ currentTickCount, dayNumber }) => {
		logger.info("A NEW DAY: ", {
			currentTickCount,
			dayNumber,
		});
		// Place babies
		placeBabies();
		// get entity list from ent mgr - clone this list, sort it in however order you want.. e.g. initiative.  Exclude babies
		const entities = getSortedEntitiesList();
		entities.forEach(e => {
			entityActions(e);
		});
		// Refresh avatars
		envMgr.refreshAvatars(entityMgr.showEntities());
		//// END NEW SHIT
	};

	// during the night, things rest.  Current babies grow, new babies are born, and everyone gets a reset on their actions
	const nightCycle = () => {
		logger.debug("Time to sleep");
		// new babies born
		newBabies = entityMgr.deliver();
		// parents grow babies (this is the incubation step)
		entityMgr.incubate();
		// new initiatives for everyone
		// NOTE: Moving the below to end of day cycle, on Mon 04/25/22
		// // Refresh avatars
		// envMgr.refreshAvatars(entityMgr.showEntities());

		// NOTE: The order of the above matters, since if new babies are born first they'll grow immediately.  And refreshing avatars changes their state so that should happen last
	};

	init();

	return {
		run,
		dayCycle,
		nightCycle,
	};
};

export default Updater;
