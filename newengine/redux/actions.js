import { environmentSlice, entitySlice, gameSlice } from "./slices";

export default {
	...entitySlice.actions,
	...environmentSlice.actions,
	...gameSlice.actions,
};
