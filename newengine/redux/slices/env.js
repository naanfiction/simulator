import { createSlice } from "@reduxjs/toolkit";

import config from "../../conf";

import Symbols from "../../../supp/Symbols";
import MathEngine from "../../state/MathEngine";

import {
	createAvatarGivenEntity /* findIdInEnvironment */,
} from "../../miscFunctions";

// NEW 12/22/21

const environmentSlice = createSlice({
	name: "env",
	initialState: {
		environment: [],
		width: config.envWidth,
		height: config.envHeight,
		locationIndex: {},
	},
	reducers: {
		setEnvironment(state, action) {
			state.environment = action.payload;
		},
		placeAvatarInEnvironment(state, action) {
			const { avatar, row, column } = action.payload;
			const envClone = JSON.parse(JSON.stringify(state.environment));
			envClone[row][column] = avatar;
			// update the location index
			state.locationIndex[avatar.id] = { row, col: column };
			state.environment = envClone;
		},
		move(state, action) {
			const envClone = JSON.parse(JSON.stringify(state.environment));
			const { oldCoords, newCoords } = action.payload;
			const currentEnt = { ...envClone[oldCoords.row][oldCoords.col] };
			envClone[oldCoords.row][oldCoords.col] = Symbols.render.EMPTY_SPACE;
			envClone[newCoords.row][newCoords.col] = currentEnt;
			// update the location index
			state.locationIndex[currentEnt.id] = { ...newCoords };
			state.environment = envClone;
		},
		refreshAvatars(state, action) {
			const entitiesList = action.payload;
			const envClone = JSON.parse(JSON.stringify(state.environment));
			// loop through the environment and find each entity
			for (let row = 1; row <= state.height; row++) {
				for (let col = 1; col <= state.width; col++) {
					if (!MathEngine.isCellEmpty({ row, col }, envClone)) {
						// const cell = envClone[row][col];
						const entityFound = entitiesList.find(
							e => e.id === envClone[row][col].id
						);
						const avatar = createAvatarGivenEntity(entityFound);
						envClone[row][col] = avatar;
					}
				}
			}
			state.environment = envClone;
		},
	},
});

export default environmentSlice;
