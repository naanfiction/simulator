import log4js from "log4js";
import { createSlice } from "@reduxjs/toolkit";
// import EntityFactory from "../../state/entities/EntityFactory";
import Symbols from "../../../supp/Symbols";

const logger = log4js.getLogger();

const removeEntity = (state, action) => {
	const entitiesClone = JSON.parse(JSON.stringify(state.entities));
	const { id } = action.payload;
	const entityIndex = entitiesClone.findIndex(e => e.id === id);
	const entity = entitiesClone.splice(entityIndex, 1)[0];
	return { entity, entitiesClone };
};

const entitySlice = createSlice({
	name: "entities",
	initialState: { entities: [] },
	reducers: {
		spontaneousBirth(state, action) {
			state.entities = [action.payload, ...state.entities];
		},
		grow(state, action) {
			// Find entity with id
			const entitiesClone = JSON.parse(JSON.stringify(state.entities));
			const { id, type } = action.payload;
			const entityIndex = entitiesClone.findIndex(e => e.id === id);
			const originalSymbol = Symbols.render[type];
			if (entitiesClone[entityIndex].age < 1) {
				entitiesClone[entityIndex].age += 1;
			} else {
				// Baby is 1!  and time to become an adult
				if (!originalSymbol) {
					logger.error(
						`Matching symbol not found for entity type ${type}.  For entity: ${id}`
					);
				}
				entitiesClone[entityIndex].symbol = originalSymbol;
				delete entitiesClone[entityIndex].age;
			}
			state.entities = entitiesClone;
		},
		procreate(state, action) {
			const { entity: procreatingEntity, entitiesClone } = removeEntity(
				state,
				action
			);
			procreatingEntity.symbol = "G";
			procreatingEntity.incubate = 0;
			state.entities = [procreatingEntity, ...entitiesClone];
		},
		incubate(state, action) {
			const entitiesClone = JSON.parse(JSON.stringify(state.entities));
			const { breedingEntityIds } = action.payload;
			breedingEntityIds.forEach(breederId => {
				const entityIndex = entitiesClone.findIndex(
					e => e.id === breederId
				);
				entitiesClone[entityIndex].incubate += 1;
			});
			state.entities = entitiesClone;
		},
		// deliver(state, action) {
		//   // Deliver needs to know the direction.
		//   //   03/29/22: This store should not know of directions.  Only the Updater should know of both entities and of directions.  Move that logic up.
		//   //
		// },
		death(state, action) {
			logger.debug("Death happens to all", action);
			const { entitiesClone } = removeEntity(state, action);
			state.entities = entitiesClone;
		},
		setEntities(state, action) {
			state.entities = action.payload;
		},
	},
});

export default entitySlice;
