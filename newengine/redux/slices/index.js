import environmentSlice from "./env";
import entitySlice from "./entities";
import gameSlice from "./game";

export { environmentSlice, entitySlice, gameSlice };
