import { createSlice } from "@reduxjs/toolkit";
import config from "../../conf";

const gameSlice = createSlice({
	name: "game",
	initialState: {
		tickInterval: config.tickInterval,
		turnsToRun: config.turnsToRun,
		ticksPerTurn: config.ticksPerTurn,
		name: config.name,
	},
	reducers: {},
});

export default gameSlice;
