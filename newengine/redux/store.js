import { configureStore } from "@reduxjs/toolkit";
import { environmentSlice, entitySlice, gameSlice } from "./slices";
// NEW 12/22/21

const reducer = {
	entities: entitySlice.reducer,
	env: environmentSlice.reducer,
	game: gameSlice.reducer,
};

const store = configureStore({ reducer });

export default store;
