import ENTITY_TYPES from "./EntityTypes";

module.exports = {
	tickInterval: 1000,
	turnsToRun: 3,
	ticksPerTurn: 3,
	envWidth: 4,
	envHeight: 5,
	name: "test",
	initialPopulation: {
		// numCreaturesMap = {[ENTITY_TYPES.WANDERER]: 3, [ENTITY_TYPES.PLANT]: 1};
		[ENTITY_TYPES.WANDERER]: 2,
		[ENTITY_TYPES.PLANT]: 2,
	},
};
