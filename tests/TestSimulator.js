var sim = require("../Simulator");
const ENTITY_TYPES = require("../entities/EntityTypes");

var allTests = [];

// todo: sychronize the size of the board, from the test suite to setuptest tests
setupTests();
runTests();

// function setupTests() {
// var gameWidth = 3, gameLength = 4, numCreaturesMap = {wanderers: 3};
//   var walls = sim.createWallsLocations(gameWidth, gameLength);
//
// var test = createTest('test gameboard', function() {
//   var gameboard = sim.createGameboard(gameWidth, gameLength, numCreaturesMap);
//   console.log(gameboard);
// });
// addTestToSuite(test);
// sim.prettyPrintGameboard(walls, gameWidth + 2, gameLength + 2);
// }

function setupTests() {
	var gameWidth = 7,
		gameLength = 9,
		numCreaturesMap = {
			[ENTITY_TYPES.WANDERER]: 3,
			[ENTITY_TYPES.PLANT]: 1,
		};

	var test = createTest("test gameboard 1", function () {
		sim.setupSimulator(numCreaturesMap, gameWidth, gameLength);
		sim.setNumberOfDaysToSimulate(113);
		sim.run();
	});
	addTestToSuite(test);

	// test = createTest('test gameboard 2', function() {
	//   sim.setupSimulator(numCreaturesMap, null, null);
	//   sim.setNumberOfDaysToSimulate(1);
	//   sim.run();
	// });
	// addTestToSuite(test);
}

function runTests() {
	for (var test of allTests) {
		console.log("running test: ", test.name);
		test.run();
	}
}

function Test(name) {
	this.name = name;
}

Test.prototype.run = function () {
	throw new error("implement me!");
};

function createTest(name, code) {
	var theTest = new Test(name);
	theTest.run = code;

	return theTest;
}

function addTestToSuite(test) {
	allTests.push(test);
}
