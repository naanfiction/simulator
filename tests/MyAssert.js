function equal(expected, actual) {
	if (expected != actual) {
		throw new Error(
			"expected: " + expected + " is not equal to actual: " + actual
		);
	}
}

module.exports = {
	equal: equal,
};
