import test from "ava";
const WANDERER = require("../../entities/Wanderer");
const DIRECTIONS = require("../../supp/Directions");
const UTILS = require("../../supp/Utils");

test("test wanderer act without direction", t => {
	var expectedResult = undefined;
	var entity = new WANDERER.Wanderer();
	var actualResult = entity.act();
	t.is(expectedResult, actualResult);
});
