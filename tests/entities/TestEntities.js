import test from "ava";
const WALL = require("../../entities/Wall");
const ENTITY = require("../../entities/Entity");
const WANDERER = require("../../entities/Wanderer");

test("test wall avatar", t => {
	testEntityAvatar(t, new WALL.Wall(), "#");
});

test("test wanderer avatar", t => {
	testEntityAvatar(t, new WANDERER.Wanderer(), "o");
});

test("test random direction picked to travel is a valid direction (in Symbols.directions)", t => {
	// Start with a full template
	// Add Empty Space to north of us
	var entity = new ENTITY.Entity();
	var direction = entity.pickRandomDirection();
	t.true(direction in Symbols.directions);
});

test("test should detect that nearby is full", t => {
	var expectedResult = true;
	// give a nearby, expect to be full
	var wanderer = new WANDERER.Wanderer();
	wanderer.look(createFullNearby());
	var actualResult = wanderer.isAllNearbyFull();
	t.is(expectedResult, actualResult);
});

test("test that nearby is not full as long as there is at least one empty", t => {
	var expectedResult = false;
	// Start with a full template
	var nearbyWithSpace = createFullNearby();
	// Add Empty Space to north of us
	nearbyWithSpace.N = Symbols.render.EMPTY_SPACE;
	var wanderer = new WANDERER.Wanderer();
	wanderer.look(nearbyWithSpace);
	var actualResult = wanderer.isAllNearbyFull();
	t.is(expectedResult, actualResult);
});

test("test wanderer does nothing if it cannot move anywhere", t => {
	var expectedResult = undefined;
	// create a full nearby
	var fullNearby = createFullNearby();
	var wanderer = new WANDERER.Wanderer();
	wanderer.look(fullNearby);
	var spaceDecided = wanderer.pickEmptySpaceNearby();
	t.is(expectedResult, spaceDecided);
});

test("test only chooses the last empty space when only 1", t => {
	var expectedResult = Symbols.directions.E;
	// create a full nearby
	var almostFullNearby = createFullNearby();
	almostFullNearby.E = Symbols.render.EMPTY_SPACE;
	var wanderer = new WANDERER.Wanderer();
	wanderer.look(almostFullNearby);
	var spaceDecided = wanderer.pickEmptySpaceNearby();
	t.is(expectedResult, spaceDecided);
});

test("test only chooses a correct empty space", t => {
	var expectedResults = [Symbols.directions.E, Symbols.directions.W];
	// create a full nearby
	var almostFullNearby = createFullNearby();
	almostFullNearby.E = almostFullNearby.W = Symbols.render.EMPTY_SPACE;
	var wanderer = new WANDERER.Wanderer();
	wanderer.look(almostFullNearby);
	var spaceDecided = wanderer.pickEmptySpaceNearby();
	t.true(expectedResults.includes(spaceDecided));
});

function testEntityAvatar(t, entity, expectedAvatar) {
	t.plan(1);
	t.is(expectedAvatar, entity.avatar);
}

function createFullNearby() {
	var nonEmptyMarker = "o";
	return {
		[Symbols.directions.N]: nonEmptyMarker,
		[Symbols.directions.E]: nonEmptyMarker,
		[Symbols.directions.S]: nonEmptyMarker,
		[Symbols.directions.W]: nonEmptyMarker,
	};
}
