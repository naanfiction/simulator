[33m[nodemon] 1.19.4[39m
[33m[nodemon] to restart at any time, enter `rs`[39m
[33m[nodemon] watching dir(s): *.*[39m
[33m[nodemon] watching extensions: js,mjs,json[39m
[32m[nodemon] starting `babel-node ./newengine/server.js`[39m
Starting Engine
Init Updater
Init EntityManager
Init Environment Manager
Populating 2 species of type PLANT
Placing 0 at 4, 1
Placing 1 at 2, 3
Populating 2 species of type WANDERER
Placing 2 at 1, 1
Placing 3 at 1, 1
-- CLOCK STARTING --
Board: 
# # # # # #
# o - - - #
# - - * - #
# - - - - #
# * - - - #
# - - - - #
# # # # # #
## A New Day:  { currentTickCount: 1, dayNumber: 1 }
Entity 2 taking move action
[31m[nodemon] app crashed - waiting for file changes before starting...[39m
